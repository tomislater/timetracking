from __future__ import print_function
from setuptools import setup

import timetracking


setup(
    name='timetracking',
    version=timetracking.__version__,
    url='https://bitbucket.org/tomislater/timetracking',
    license='MIT License',
    author='Tomasz Święcicki',
    author_email='tomislater@gmail.com',
    description='Timetracking',
    packages=['timetracking'],
    include_package_data=True,
    platforms='any',
)
