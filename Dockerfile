FROM python
ADD . /timetracking
WORKDIR /timetracking
RUN pip install -r requirements.txt