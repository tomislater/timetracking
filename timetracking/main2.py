import json

import jira
import click


@click.command()
@click.option('--issues', help='List of issues.')
@click.option('--version', help='Fix version - ID!.')
def add_fix_version(issues, version):

    # Get config
    with open('configs/' + 'config.json', 'r') as f:
        config = json.load(f)

    # Connect to the first JIRA
    jira_config = config['second_jira']
    _jira = jira.JIRA(jira_config['url'], basic_auth=(jira_config['login'], jira_config['password']))

    # get version
    version = _jira.version(version)

    # update issues
    for issue in issues.split():
        ticket = _jira.issue(issue)
        versions = [v.raw for v in ticket.fields.fixVersions]
        ticket.update(fields={"fixVersions": versions + [version.raw]})

        # do the same thing for parent
        if hasattr(ticket.fields, "parent"):
            parent = _jira.issue(ticket.fields.parent.id)
            versions = [v.raw for v in parent.fields.fixVersions]
            parent.update(fields={"fixVersions": versions + [version.raw]})


if __name__ == '__main__':
    add_fix_version()
