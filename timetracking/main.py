import json

import jira
import click


@click.command()
@click.option('--issue', help='The issue.')
@click.option('--time', help='The time.')
@click.option('--comment', default='', help='The comment.')
def add_worklog(issue, time, comment):

    # Get config
    with open('configs/' + 'config.json', 'r') as f:
        config = json.load(f)

    # Connect to the first JIRA
    first_jira_config = config['first_jira']
    first_jira = jira.JIRA(first_jira_config['url'], basic_auth=(first_jira_config['login'], first_jira_config['password']))

    # Connect to the second JIRA
    second_jira_config = config['second_jira']
    second_jira = jira.JIRA(second_jira_config['url'], basic_auth=(second_jira_config['login'], second_jira_config['password']))

    # Add worklog
    first_worklog = first_jira.add_worklog(first_jira_config['home_issue'], timeSpent=time, comment=comment)
    second_worklog = second_jira.add_worklog(issue, timeSpent=time, comment=comment)

    print("First worklog - {0} - {1}".format(first_jira_config['url'], first_worklog))
    print("Second worklog - {0} - {1}".format(second_jira_config['url'], second_worklog))


if __name__ == '__main__':
    add_worklog()
