# timetracking

If you are tired typing a log work in two jiras, you can use it! Look at the examples below.

## Setup

In ``configs`` you have to create own config file. There is an example.
Just copy it and create your own.

It must be called ``config.json``.

    cp configs/example_config.json configs/config.json

Build an image:

    docker build -t timetracking .


## Examples

Add worklog to issue **DB-1234**, 5 minutes:

    docker run timetracking python timetracking/main.py --issue DB-1234 --time 5m

Add worklog to issue **DB-79**, 3 hours and 33 minutes:

    docker run timetracking python timetracking/main.py --issue DB-79 --time "3h 33m"

Add worklog to issue **DB-235**, 1 hour plus comment:

    docker run timetracking python timetracking/main.py --issue DB-235 --time 1h --comment "Pair programming with someone"

Add version for some tickets:

    docker run timetracking python timetracking/main2.py --version 13005 --issues "DB-5340 DB-5442 DB-5434 DB-5431 DB-5433"

